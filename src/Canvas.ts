import { createCanvas } from "canvas";
import { config } from "./Main";

export const globCanvas = createCanvas(config.canvasSizeX, config.canvasSizeY)
export const globCtx = globCanvas.getContext('2d')

globCtx.strokeStyle = `#000000`;
globCtx.fillStyle = `#000000`;
globCtx.fillRect(0,0,config.canvasSizeX, config.canvasSizeY)




