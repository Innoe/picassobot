import {
    Discord,
    CommandMessage,
    Command,
    On,
    ArgsOf,
    Guard,
} from "@typeit/discord";
import { MessageAttachment } from "discord.js";
import { globCtx, globCanvas } from "./Canvas";

import { CheckCanvasBounds, CheckColor, NotBot } from "./Guards";
import { config } from "./Main";

@Discord(config.commandPrefix)
export abstract class Commands {

    @Command("ping")
    @Guard(NotBot)
    ping(command: CommandMessage): void {
        command.reply("pong!");
    }


    @Command("line :color :x1 :y1 :x2 :y2")
    @Guard(CheckCanvasBounds, CheckColor)
    line(command: CommandMessage): void {
        let { color, x1, x2, y1, y2 } = command.args;
        globCtx.strokeStyle = `${color}`;
        globCtx.fillStyle = `${color}`;
        globCtx.beginPath()
        globCtx.lineTo(x1, y1)
        globCtx.lineTo(x2, y2)
        globCtx.stroke()
        command.reply("", new MessageAttachment(globCanvas.toBuffer()))
    }

    @Command("fillRect :color :x1 :y1 :x2 :y2")
    @Guard(CheckCanvasBounds, CheckColor)
    fillRect(command: CommandMessage): void {
        let { color, x1, x2, y1, y2 } = command.args;
        globCtx.strokeStyle = `${color}`;
        globCtx.fillStyle = `${color}`;
        globCtx.fillRect(x1, y1, x2, y2);
        command.reply("", new MessageAttachment(globCanvas.toBuffer()))
    }

    @Command("rect :color :x1 :y1 :x2 :y2")
    @Guard(CheckCanvasBounds, CheckColor)
    rect(command: CommandMessage): void {
        let { color, x1, x2, y1, y2 } = command.args;
        globCtx.strokeStyle = `${color}`;
        globCtx.fillStyle = `${color}`;
        globCtx.beginPath()
        globCtx.rect(x1, y1, x2, y2);
        globCtx.stroke();
        command.reply("", new MessageAttachment(globCanvas.toBuffer()))
    }

    @Command("circle :color :x1 :y1 :r")
    @Guard(CheckCanvasBounds, CheckColor)
    circle(command: CommandMessage): void {
        let { color, x1, r, y1 } = command.args;
        globCtx.strokeStyle = `${color}`;
        globCtx.fillStyle = `${color}`;
        globCtx.beginPath()
        globCtx.arc(x1, y1, r, 0, 2 * Math.PI);
        globCtx.stroke()
        command.reply("", new MessageAttachment(globCanvas.toBuffer()))
    }

    @Command("fillCircle :color :x1 :y1 :r")
    @Guard(CheckCanvasBounds, CheckColor)
    fillCircle(command: CommandMessage): void {
        let { color, x1, r, y1 } = command.args;
        globCtx.strokeStyle = `${color}`;
        globCtx.fillStyle = `${color}`;
        globCtx.beginPath();
        globCtx.arc(x1, y1, r, 0, 2 * Math.PI);
        globCtx.closePath();
        globCtx.fill();
        command.reply("", new MessageAttachment(globCanvas.toBuffer()))
    }

    @Command("clear :color")
    @Guard(CheckColor)
    clear(command: CommandMessage): void {
        let { color } = command.args;
        globCtx.fillStyle = `${color}`;
        globCtx.fillRect(0,0,config.canvasSizeX, config.canvasSizeY)
        command.reply("Canvas cleared")
    }

}
