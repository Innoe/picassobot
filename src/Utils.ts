import { config } from "./Main";


export const getBoundAsString = () => `(${config.canvasSizeX}, ${config.canvasSizeY})`