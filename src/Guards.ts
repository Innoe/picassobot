import { GuardFunction } from "@typeit/discord";
import { config } from "./Main";
import { getBoundAsString } from "./Utils";

export const NotBot: GuardFunction<"message"> = async (
  [message],
  client,
  next
) => {
  if (!message.author.bot) {
    await next();
  }
};


const checkForValidNumber = (args: any, argName: string) => {
  return Object.keys(args).includes(argName) && (typeof args[argName] != "number" || args[argName] > config.canvasSizeX || args[argName] < 0)
}


export function CheckArgs(argsCound: number) {
  const guard: GuardFunction<"commandMessage"> = (
    [message],
    client,
    next
  ) => {
  };

  return guard;
}


export const CheckColor: GuardFunction<"commandMessage"> = async (
  [message],
  client,
  next
) => {
  if(/^#[0-9A-F]{6}$/i.test(message.args.color)){
    await next();
    return;
  }
  message.reply("Enter a valid color");
};


export const CheckCanvasBounds: GuardFunction<"commandMessage"> = async (
  [message],
  client,
  next
) => {
  let checkedArgs=["x1", "x2", "y1", "y2", "r"];

  for(let a of checkedArgs){
    if(checkForValidNumber(message.args, a)){
      message.reply(`${a} must be in the bound of ` + getBoundAsString());
      return;
    }
  }
  
  await next();
};
